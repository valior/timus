#!/usr/bin/env python3

import sys

a, b = list(input().split())
  
print(int(a) + int(b))
